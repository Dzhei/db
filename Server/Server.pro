TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    multyserver.cpp \
    dbserver.cpp

HEADERS += \
    multyserver.h \
    config.h \
    dbserver.h
