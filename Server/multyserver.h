#ifndef MULTYSERVER_H
#define MULTYSERVER_H

#include <iostream>
#include <algorithm>
#include <set>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

#include <sys/epoll.h>

#include <errno.h>
#include <string.h>

#include "config.h"

class MultyServer
{
protected:
    struct epoll_event * Events;
    int MasterSocket;

    int set_nonblock(int fd);
    virtual void listeningFromSlaveDO(int i);

public:
    MultyServer(int masterPort);
};

#endif // MULTYSERVER_H
