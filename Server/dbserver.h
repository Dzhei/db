#ifndef DBSERVER_H
#define DBSERVER_H

#include "multyserver.h"

class DBServer:protected MultyServer
{
protected:
    virtual void listeningFromSlaveDO(int i);
public:
    DBServer (int port):MultyServer(port){}
};

#endif // DBSERVER_H
