#ifndef CONFIG_H
#define CONFIG_H

#define MAX_EVENTS 32
#define PORT 12345
#define BUF_SIZE 1024

#endif // CONFIG_H
