#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>


#define PORT 3425

class Listener
{
private:
    int sock;
    int listener;
    int port;
    void CreateNet();
public:
    explicit Listener (unsigned int port);
    void Listen();
};

Listener::Listener(unsigned int port)
{
    struct sockaddr_in addr;
    this->port=port;
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }
}

void Listener::Listen()
{
    char buf[1024];
    int bytes_read;

    listen(listener, 1);
    //CreateNet() should be in threads
    CreateNet();
}

void Listener::CreateNet()
{
    sock = accept(listener, NULL, NULL);
    if(sock < 0)
    {
        perror("accept");
        exit(3);
    }
    while (1)
    {
        bytes_read = recv(sock, buf, 1024, 0);
        if(bytes_read <= 0) break;
        std::cout<<buf<<std::endl;
        send(sock, buf, bytes_read, 0);
    }
}

int main()
{
    Listener * Server = new Listener(PORT);
    Server->Listen();
    return 0;
}
